# ASCON cipher and ARX ciphers - Research - SW Implementation - Attacks

### ARX ciphers
The term "ARX" refers to the fundamental operations used in the cipher's round function: addition (A), rotation (R), and XOR (X). These operations are applied in various combinations to achieve diffusion and confusion in the encryption process.

### MAC (Message authentication code)
In cryptography, a message authentication code (MAC), sometimes known as an authentication tag, is a short piece of information used for authenticating and integrity-checking a message. In other words, to confirm that the message came from the stated sender (its authenticity) and has not been changed (its integrity). The MAC value allows verifiers (who also possess a secret key) to detect any changes to the message content. 


## Brief information about ciphers and related publications

### SipHash
SipHash is ARX-based pseudorandom function optimized for short inputs. It was developed as a hash table lookup function, but it is also used for **MAC generation**.
#### Implementation

#### Articles
- [Correlation Power Analysis of SipHash](https://ieeexplore.ieee.org/abstract/document/9770139)
	- Matúš' article
	- CPA attack on SipHash on STM32F (ARM)
	- ChipWhisperer utilized

#### Useful citations
- TBA
******************************************************************************************************************************************************************************************


### ASCON
ASCON (Authenticated Sponge Construction) is a lightweight authenticated **encryption algorithm**. It is designed to provide both encryption and authentication of data with a focus on efficiency and security. It is suitable for use in constrained environments such as embedded systems or IoT devices. It is designed to resist attacks such as differential and linear cryptanalysis.
#### Implementation
- TBA
#### Articles
- [Efficient Second-Order Masked Software Implementations of Ascon in Theory and Practice](https://ascon.iaik.tugraz.at/files/ascon-masked-implementation.pdf)
	- attack on masked SW implementation on ARM using ChipWhisperer suite
#### Useful citations
- TBA
******************************************************************************************************************************************************************************************


### ChaCha20
ChaCha20 is a symmetric **encryption algorithm**, based on a variant of Salsa20, that belongs to the family of stream ciphers. It operates on 512-bit blocks and supports key sizes of 128, 256, or 512 bits. ChaCha20 is widely used due to its speed and security properties.

The 20-round stream cipher ChaCha/20 is consistently faster than AES and is recommended by the designer for typical cryptographic applications. The reduced-round ciphers ChaCha/12 and ChaCha/8 are among the fastest 256-bit stream ciphers available and are recommended for applications where speed is more important than confidence. [Crypto++](https://cryptopp.com/wiki/ChaCha20)

## Notes regarding the implementation

The ChaCha20 Block Function (ChaCha6, ChaCha7,.. have different Block functions)

   The ChaCha block function transforms a ChaCha state by running
   multiple quarter rounds.

   The inputs to ChaCha20 are:

   -  A 256-bit key, treated as a concatenation of eight 32-bit little-
      endian integers.

   -  A 96-bit nonce, treated as a concatenation of three 32-bit little-
      endian integers.

   -  A 32-bit block count parameter, treated as a 32-bit little-endian
      integer.

   The output is 64 random-looking bytes.

Source: [RFC 7539](https://datatracker.ietf.org/doc/html/rfc7539)

#### Implementation
- [D. J. Bernstein's reference implementation](https://cr.yp.to/chacha.html)
	- used in ChipWhisperer, because OpenSSL has lot of dependencies
- [OpenSSL](https://github.com/openssl/openssl/blob/master/crypto/chacha/chacha_enc.c)
- [ChaCha20 C implementation](https://github.com/Ginurx/chacha20-c)

#### Articles
- [D. J. Bernstein's information about performed attacks](https://cr.yp.to/streamciphers/attacks.html#chacha20)
- [Chacha20 reference](https://www.rfc-editor.org/rfc/rfc8439)
	- implementation details
- [Bricklayer Attack: A Side-Channel Analysis on the ChaCha Quarter Round](https://link.springer.com/chapter/10.1007/978-3-319-71667-1_4#ref-CR7)
	- attack on OpenSSL implementation on 32bit ARM and assembly implementation
	- usage of Correlation Electromagnetic Analysis
	
- [Side channel attack on the ChaCha cipher (FIT CTU)](https://dspace.cvut.cz/handle/10467/77289)
	- DPA and EMA on AVR in chip card
	
- [Physical side-channel analysis of ChaCha20-Poly1305](https://ieeexplore.ieee.org/abstract/document/7927155)
	- EMA, analyze of the effectiveness of randomly shuffling the operations of the ChaCha round function

#### Useful citations
- ChaCha20 citation
```
@inproceedings{bernstein2008chacha,
  title={ChaCha, a variant of Salsa20},
  author={Bernstein, Daniel J and others},
  booktitle={Workshop record of SASC},
  volume={8},
  number={1},
  pages={3--5},
  year={2008},
  organization={Citeseer}
}
```
******************************************************************************************************************************************************************************************


### BLAKE(2)
The original BLAKE **hash function**, which BLAKE2 succeeded, is an ARX cipher. However, BLAKE2 is also a hash function optimized for performance and security, but it does not fall compeletly under the category of ARX ciphers because it also contains compression funcion.

BLAKE2b and BLAKE2s are designed to be efficient on a single CPU core. BLAKE2s is optimized for 8- to 32-bit platforms and produces digests of any size between 1 and 32 bytes.
#### Implementation
- [Official BLAKE2 implementation](https://github.com/BLAKE2/BLAKE2)
- [Official BLAKE2 website with 3rd-party SW links](https://www.blake2.net/)

#### Articles
- At [Official BLAKE2 website](https://www.blake2.net/) under **Cryptoanalysis**
#### Useful citations
- TBA
******************************************************************************************************************************************************************************************


## Other useful articles
- [General Framework for Evaluating LWC Finalists in Terms of Resistance to Side-Channel Attacks](https://csrc.nist.gov/csrc/media/Events/2022/lightweight-cryptography-workshop-2022/documents/papers/general-framework-for-evaluating-lwc-finalists-in-terms-of-resistance-to-side-channel-attacks.pdf)
	- mention of ChipWhisperer
	- how-to evaluate LWC resistance to side-channel attacks 
