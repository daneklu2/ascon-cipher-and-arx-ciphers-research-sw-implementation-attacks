# ARX ciphers - Research - SW Implementation - Attacks

## Current state of article for VyLet 2024
The first phase of the project is complete. The draft of the paper can be found [here](./docs/VyLet2024-daneklu2.pdf)

## General information

### ARX ciphers
The term "ARX" refers to the fundamental operations used in the cipher's round function: addition (A), rotation (R), and XOR (X). These operations are applied in various combinations to achieve diffusion and confusion in the encryption process.

### MAC (Message authentication code)
In cryptography, a message authentication code (MAC), sometimes known as an authentication tag, is a short piece of information used for authenticating and integrity-checking a message. In other words, to confirm that the message came from the stated sender (its authenticity) and has not been changed (its integrity). The MAC value allows verifiers (who also possess a secret key) to detect any changes to the message content. 


## Brief information about ciphers and related publications

### SipHash
SipHash is ARX-based pseudorandom function optimized for short inputs. It was developed as a hash table lookup function, but it is also used for **MAC generation**.
#### Implementation

#### Articles
- [Correlation Power Analysis of SipHash](https://ieeexplore.ieee.org/abstract/document/9770139)
	- Matúš' article
	- CPA attack on SipHash on STM32F (ARM)
	- ChipWhisperer utilized

#### Useful citations
- TBA
******************************************************************************************************************************************************************************************

### ChaCha20
ChaCha20 is a symmetric **encryption algorithm**, based on a variant of Salsa20, that belongs to the family of stream ciphers. It operates on 512-bit blocks and supports key sizes of 128, 256, or 512 bits. ChaCha20 is widely used due to its speed and security properties.

The 20-round stream cipher ChaCha/20 is consistently faster than AES and is recommended by the designer for typical cryptographic applications. The reduced-round ciphers ChaCha/12 and ChaCha/8 are among the fastest 256-bit stream ciphers available and are recommended for applications where speed is more important than confidence. [Crypto++](https://cryptopp.com/wiki/ChaCha20)

Further information about the current progress on the Cipher implementation and SCA [here](./chacha20/README.md)


### BLAKE(2)
The original BLAKE **hash function**, which BLAKE2 succeeded, is an ARX cipher. However, BLAKE2 is also a hash function optimized for performance and security, but it does not fall compeletly under the category of ARX ciphers because it also contains compression funcion.

BLAKE2b and BLAKE2s are designed to be efficient on a single CPU core. BLAKE2s is optimized for 8- to 32-bit platforms and produces digests of any size between 1 and 32 bytes.

Further information about the current progress on the Cipher implementation and SCA [here](./blake2/README.md)


## Other useful articles
- [General Framework for Evaluating LWC Finalists in Terms of Resistance to Side-Channel Attacks](https://csrc.nist.gov/csrc/media/Events/2022/lightweight-cryptography-workshop-2022/documents/papers/general-framework-for-evaluating-lwc-finalists-in-terms-of-resistance-to-side-channel-attacks.pdf)
	- mention of ChipWhisperer
	- how-to evaluate LWC resistance to side-channel attacks 
