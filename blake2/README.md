# BLAKE(2)
The original BLAKE **hash function**, which BLAKE2 succeeded, is an ARX cipher. However, BLAKE2 is also a hash function optimized for performance and security, but it does not fall compeletly under the category of ARX ciphers because it also contains compression funcion.

BLAKE2b and BLAKE2s are designed to be efficient on a single CPU core. BLAKE2s is optimized for 8- to 32-bit platforms and produces digests of any size between 1 and 32 bytes.
#### Implementation
- [Official BLAKE2 implementation](https://github.com/BLAKE2/BLAKE2)
- [Official BLAKE2 website with 3rd-party SW links](https://www.blake2.net/)

#### Articles
- At [Official BLAKE2 website](https://www.blake2.net/) under **Cryptoanalysis**
#### Useful citations
- TBA