# Notes regarding the ChipWhisperer Environment

## Python Notebook Start-up
- [Installation guide](https://chipwhisperer.readthedocs.io/en/latest/linux-install.html)
- Open Terminal and run: pyenv `pyenv activate cw`
- In chipwhisperer folder run: `jupyter notebook`

## UART communication
- `minicom` used
- Ctrl + A and then pressing Z opens Settings
- U settings (adding CR to LF) needed

## TODO
Integer values are raw readings from the ChipWhisperer ADC. The ChipWhisperer-Lite has a 10-bit ADC, the Nano has an 8-bit ADC, and the Husky can read either 8-bits or 12-bits of ADC data.
- use uint16 instead uit8 - loosing precision...


Baud-rate = 38400
Encryption count: 10, Total duration: 1.0502424240112305, Average duration: 0.10502424240112304 seconds
Encryption count: 100, Total duration: 10.452462673187256, Average duration: 0.10452462673187256 seconds
Encryption count: 1000, Total duration: 104.75829315185547, Average duration: 0.10475829315185547 seconds
Encryption count: 10000, Total duration: 1047.2735912799835, Average duration: 0.10472735912799835 seconds

Baud-rate = 230400
Encryption count: 10, Total duration: 0.2869894504547119, Average duration: 0.028698945045471193 seconds
Encryption count: 100, Total duration: 2.901087760925293, Average duration: 0.02901087760925293 seconds
Encryption count: 1000, Total duration: 29.695407390594482, Average duration: 0.029695407390594484 seconds

(ChipWhisperer Scope WARNING|File _OpenADCInterface.py:730) Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0b
(ChipWhisperer Scope WARNING|File _OpenADCInterface.py:730) Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 08

Target timed out!
Encryption count: 10000, Total duration: 302.9002318382263, Average duration: 0.030290023183822633 seconds
