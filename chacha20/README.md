# ChaCha20
ChaCha20 is a symmetric **encryption algorithm**, based on a variant of Salsa20, that belongs to the family of stream ciphers. It operates on 512-bit blocks and supports key sizes of 128, 256, or 512 bits. ChaCha20 is widely used due to its speed and security properties.

The 20-round stream cipher ChaCha/20 is consistently faster than AES and is recommended by the designer for typical cryptographic applications. The reduced-round ciphers ChaCha/12 and ChaCha/8 are among the fastest 256-bit stream ciphers available and are recommended for applications where speed is more important than confidence. [Crypto++](https://cryptopp.com/wiki/ChaCha20)

## My Work
- Current Work-In-Progress repository with FW for STM32F3 + XMEGA [here](https://gitlab.fit.cvut.cz/daneklu2/chipwhisperer-firmware#) - branch [chacha20-base](https://gitlab.fit.cvut.cz/daneklu2/chipwhisperer-firmware/-/tree/chacha20-base?ref_type=heads#)

- Current Work-In-Progress Jupyter Notebooks with implemented base Non-Specific T-Test and CPA attack [here](TODO) - branch TODO


## Notes regarding the implementation

The ChaCha20 Block Function (ChaCha6, ChaCha7,.. have different Block functions)

   The ChaCha block function transforms a ChaCha state by running
   multiple quarter rounds.

   The inputs to ChaCha20 are:

   -  A 256-bit key, treated as a concatenation of eight 32-bit little-
      endian integers.

   -  A 96-bit nonce, treated as a concatenation of three 32-bit little-
      endian integers.

   -  A 32-bit block count parameter, treated as a 32-bit little-endian
      integer.

   The output is 64 random-looking bytes.

Source: [RFC 7539](https://datatracker.ietf.org/doc/html/rfc7539)

## Implementation
- [D. J. Bernstein's reference implementation](https://cr.yp.to/chacha.html)
	- used in ChipWhisperer, because OpenSSL has lot of dependencies
- [OpenSSL](https://github.com/openssl/openssl/blob/master/crypto/chacha/chacha_enc.c)
- [ChaCha20 C implementation](https://github.com/Ginurx/chacha20-c)

## Articles
- [D. J. Bernstein's information about performed attacks](https://cr.yp.to/streamciphers/attacks.html#chacha20)
- [Chacha20 reference](https://www.rfc-editor.org/rfc/rfc8439)
	- implementation details
- [Bricklayer Attack: A Side-Channel Analysis on the ChaCha Quarter Round](https://link.springer.com/chapter/10.1007/978-3-319-71667-1_4#ref-CR7)
	- attack on OpenSSL implementation on 32bit ARM and assembly implementation
	- usage of Correlation Electromagnetic Analysis
	
- [Side channel attack on the ChaCha cipher (FIT CTU)](https://dspace.cvut.cz/handle/10467/77289)
	- DPA and EMA on AVR in chip card
	
- [Physical side-channel analysis of ChaCha20-Poly1305](https://ieeexplore.ieee.org/abstract/document/7927155)
	- EMA, analyze of the effectiveness of randomly shuffling the operations of the ChaCha round function

## Useful citations
- ChaCha20 citation
```
@inproceedings{bernstein2008chacha,
  title={ChaCha, a variant of Salsa20},
  author={Bernstein, Daniel J and others},
  booktitle={Workshop record of SASC},
  volume={8},
  number={1},
  pages={3--5},
  year={2008},
  organization={Citeseer}
}
```